<div id="results">

	<button id="start">Start Genetic Algorithm</button>

	<p>Polynomial function: f(x) = a + bx + cx2 + dx3
 		+ ex4 + fx</p>

	<p>Using X value as: 0.16837284482758727</p>

	<p>We want Y to be: 841.7108319251483</p>

	<h1>Accepted Mutations</h1>

	<p>When fitter genes are found, we present them below:</p>

	<div id="closestValue">

		<h2>Closest Value found is:</h2>

		<span id="closestValRes"></span>

		<h2>Closest Fitness:</h2>

		<span id="closestFitnessRes"></span>

		<h2>Closest coefficients are:</h2>

		<span id="closestNumbers"></span>

	</div>

	<h2>Mutations</h2>

	<div id="appendResults">

	</div>

</div>
