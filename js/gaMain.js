/**
 * This function take an existing array, and replaces a char
 * at the set index
 */
function setCharAt(str,index,chr) {
  
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

/**
 * This function generates a set of completely random solutions
 */
function getRandomChromosomes() {

	var randomChromosomes = {};

	for(var i = 0; i < numberOfRandomChromosomes; i++) {

		newChromosome = {};

		for(var c = 0; c < numberOfGenesPerChromosome; c++) {

			newGene = [];

			for(var b = 0; b < numberOfBitsPerGene; b++) {

				newGene[b] = Math.floor(Math.random()*2);
			}

			newChromosome[c] = newGene;
		}

		randomChromosomes[i] = newChromosome;
	}

	return randomChromosomes;
}

/**
 * This is where we start, and loop through with each net set of solutions
 */
function startGeneticAlgorithm(possibleSolutions) {

	iteration++;

	$.each(possibleSolutions, function(key, value) {

		thisSolution = decodeSolution(value);

		allUsedChromosomes[thisSolution.string] = thisSolution;

		if(solution = checkIfWeHaveAWinner(value)) {

			console.log("boom!");

			solutionFound = true;

			return solution;
		}

	});

	if(!solutionFound) {

		if(iteration <= maxIterations) {

			console.log("Iteration: " + iteration + " and best fitness now: " + fittestChromosomeFitness);
		
			var newSolutions = generateMutations();

		} else {

			console.log("we have reached our max number of iterations :(");
			console.log(fittestSolution);
		}
	}

	return false;
}

/**
 * This function generates our mutations, however, and whatever way we choose.
 * In this example, we use a simple crossover solution AND a finer grained bit
 * crossover method
 */
function generateMutations() {

	// we will generate 3 mutations, we will use the fittest solution so
	// far, and then use tournament selection as the 2nd chromosome to
	// mutate with

	// 1. a simple crossover mutation

	// 2. a 1 bit crossover.. i.e. we'll only swap 1 bit

	var newSolutions 		= {};

	var solution1 			= fittestSolution; 
	var solution2			= tournamentSelectSolution();

	var simpleCrossovers 	= createSimpleCrossovers(solution1, solution2);
	var oneBitMutations 	= createBitCrossoverMutations(solution1, solution2);

	$.each(simpleCrossovers, function(key, value){

		newSolutions[value.string] 			= value;
	});

	$.each(oneBitMutations, function(key, value){

		newSolutions[value.string] 			= value;
	});

	startGeneticAlgorithm(newSolutions);
}

/**
 * This function creates simple crossover mutations by splitting 2 solutions
 * in half, and making 2 new solutions by swapping halves
 */
function createSimpleCrossovers(solution1, solution2) {

	var intHalfOfChromosome = (solution1.string.length / 2);

	// split each solution in half

	var solution1Part1 		= [solution1.string.slice(0, intHalfOfChromosome)];
	var solution1Part2 		= [solution1.string.slice(intHalfOfChromosome, solution1.string.length)];

	var solution2Part1 		= [solution2.string.slice(0, intHalfOfChromosome)];
	var solution2Part2 		= [solution2.string.slice(intHalfOfChromosome, solution2.string.length)];

	var hybrid1 			= solution1Part1.toString() + solution2Part2.toString();
	var hybrid2 			= solution2Part1.toString() + solution1Part2.toString();

	if(debug) {
		
		console.log("We took: " + solution1.string);
		console.log("And we took: " + solution2.string);

		console.log("And created mutation 1: " + hybrid1);
		console.log("And created mutation 2: " + hybrid2);
		
	}

	var newMutation1 = createNewChromosome(hybrid1);
	var newMutation2 = createNewChromosome(hybrid2);

	var newMutations = {};
	newMutations[newMutation1.string] = newMutation1;
	newMutations[newMutation2.string] = newMutation2;
	
	return newMutations;
}

/**
 * This function handles all bit crossover mutations
 */
function createBitCrossoverMutations(solution1, solution2) {

	var oneBitCrossovers = {};

	for(var i = 0; i < numberOf1BitCrossovers; i++) {

		var mutations = createBitCrossover(solution1, solution2);
		
		$.each(mutations, function(key, value){
		
			oneBitCrossovers[value.string] = value;
		});
	}


	return oneBitCrossovers;

}

/**
 * This function checks if the bits are identical, if they are, they both 
 * get changed to a different value
 */
function checkNotSameBit(char1, char2) {

	var returnChar = char2;

	if(char1 == char2) {

		if(char2 == 0) {

			returnChar = 1;
	
		} else {

			returnChar = 0;
		}
	} 

	return returnChar;
}

/**
 * This function performs the swap of bits
 */
function swapBits(newString2, newString1) {

	var randomInt 	= Math.floor((Math.random()* totalBits)+1)

	var bit1 		= checkNotSameBit(newString2[randomInt], newString1[randomInt]);

	var bit2 		= checkNotSameBit(newString1[randomInt], newString2[randomInt]);

	var returnObj 	= {

		'new1'	: setCharAt(newString1, randomInt, bit2),
		'new2'	: setCharAt(newString2, randomInt, bit1) 
	} ;

	return returnObj;
}


/**
 * This function allows a more finer grained mutation process than the
 * simple crossover method. We can specify how many bits to swap from
 * solution1 to solution2. The bits are picked at random.
 */
function createBitCrossover(solution1, solution2) {

	var newMutations 		= {};

	var solution1array 		= solution1.string.split();

	var solution2array 		= solution2.string.split();

	var newString1 			= solution1.string; 

	var newString2 			= solution2.string;

	var new1 				= {};

	var new2 				= {};

	for(var i = 0; i < numberOfBitsToCrossover; i++) {

		var newSolutionsObj = swapBits(newString2, newString1); 
	}

	var newSolution1 		= createNewChromosome(newSolutionsObj.new1);

	var newSolution2 		= createNewChromosome(newSolutionsObj.new2);

	newMutations[newSolution1.string] = newSolution1;
	newMutations[newSolution2.string] = newSolution2;

	return newMutations;
}

/**
 * This function takes a new mutation string, and splits it into the
 * individual chromosome parts and then returns the object
 */ 
function createNewChromosome(mutation) {

	var i = 0;

	var newChromosome = {};

	for(var c = 0; c < numberOfGenesPerChromosome; c++) {

		var newGene = [];

		for(var b = 0; b < numberOfBitsPerGene; b++) {

			newGene[b] = parseInt(mutation.slice(i, (i+1)));

			i++;
		}

		newChromosome[c] = newGene;
	}

	return newChromosome;
}

/**
 * This function picks some random solutions (this stops us from getting stuck
 * in a corner) from our collection
 * and then returns the fittest from them
 */
function tournamentSelectSolution() {

	// tournament selection - we select some random chromosomes

	var localCopyOfChromosomes = allUsedChromosomes;

	delete localCopyOfChromosomes[fittestChromosome];

	var sizeOfLocalCopy 	= Object.size(localCopyOfChromosomes) -1;

	// remove the fittest one.. as we don't want to crossover with ourself

	var usedRandomNumbers 	= [];

	var randomSelection 	= {};

	// reformat our chromosome collection so it's index starting from 0 ...
	
	// instead of using our chromosome string as the index

	var indexedCollection 	= getCorrectlyIndexedFormatOfChromosomes(localCopyOfChromosomes);

	for(var i = 0; i < tournamentSelectionAmount && i < sizeOfLocalCopy; i++) {

		var randomNumber 	= Math.floor((Math.random()* sizeOfLocalCopy)+1);
	
		// frustratingly we'll get a duplicate of random numbers on occasion...
		
		// but we'll turn a blind eye to it for now

		var randomSolution = indexedCollection[randomNumber];

		randomSelection[randomSolution.string] = randomSolution;
	}

	var winnerFromTournamentSelection = pickTournamentWinner(randomSelection);

	return winnerFromTournamentSelection;
}

/**
 * This function takes a collection of solutions and picks the fittest
 * from them all
 */
function pickTournamentWinner(solutions) {

	var fitest 			= 0;

	var fitestFitness 	= 1000;

	$.each(solutions, function(key, value){

		if(value.fitness < fitestFitness) {

			fitest 			= value.string;
			
			fitestFitness 	= value.fitness;
		}
	});

	return solutions[fitest];

}

/**
 * This function returns a collection of solutions indexed by
 * correct ascending integer format, instead of by their string
 * representation
 */
function getCorrectlyIndexedFormatOfChromosomes(solutions) {

	var i = 0;

	var indexedCollection = {};

	$.each(solutions, function(key, value){

		indexedCollection[i] = value;

		i++;
	});

	return indexedCollection;

}

/**
 * This function extends the Object class by giving giving it a field
 * called size that we can call upon later
 */
Object.size = function(obj) {
  
    var size = 0, key;
  
    for (key in obj) {

        if (obj.hasOwnProperty(key)) size++;
    }
  
    return size;
};

/**
 * This function displays solution information
 */
function displayNewFitterSolution() {

	var numbers = "";

	$.each(fittestSolution.numbers, function(key, value){

		numbers+= value + ", ";
	});

	$('#closestFitnessRes').html("<p>" + fittestChromosomeFitness + "%</p>");
	$('#closestNumbers').html("<p>" + numbers + "</p>");
	$('#closestValRes').html("<p>" + fittestSolution.value + "</p>");
	$('#appendResults').append("<p>" + fittestSolution.string + "</p>");
}

/**
 * This function sets the fittest solution, checks if a solution
 * is fitter than our current fittest and updates if it is. Also, if
 * it's the first solution we come across, it also becomes the fittest
 */
function setFittest(solution) {

	if(fittestChromosomeFitness == 0) {

		fittestChromosomeFitness 	= solution.fitness;
		fittestChromosome 			= solution.string;
		fittestSolution				= solution;

		displayNewFitterSolution();

	} else if(solution.fitness < fittestChromosomeFitness) {

		fittestChromosomeFitness 	= solution.fitness;
		fittestChromosome 			= solution.string;
		fittestSolution				= solution;	
	
		displayNewFitterSolution();
	}
}

/**
 * This function determines whether a solution is the winning 
 * set of coefficients or not
 */
function checkIfWeHaveAWinner(solution) {

	// lets give our winner check a little bit of margin for error

	if(solution.fitness > 0.99 && solution.fitness < 1.01) {

		return true;
	
	} else {

		return false;
	}

}

/**
 * This function takes the binary representation of a given solution
 * and returns a solution object that contains the numbers, value, and fitness
 */
function decodeSolution(solution) {

	var stringSolution 	= "";

	var numbers 		= {};

	var a 				= 0;

	$.each(solution, function(key, value){

		var powerOf = numberOfBitsPerGene;

		var thisNum = 0;

		for(var i = 0; i < numberOfBitsPerGene; i++) {

			stringSolution+= value[i];

			powerOf--;

			addInt 	= value[i] + 0.2;

			thisInt = Math.pow(addInt, powerOf); 

			thisNum+= thisInt;
		}

		numbers[a] = thisNum;

		a++;
	})

	
	value = getValueAgainstFunction(numbers);

	thisSolution = {
   
		numbers : numbers,
		string 	: stringSolution,
		value 	: value,
		fitness : getFitness(value)
	};

	setFittest(thisSolution);

	allUsedChromosomes[stringSolution] = thisSolution;

	return thisSolution;
}

/**
 * This function takes the numbers from a given solution
 * and runs it against the polynomial function and returns the value
 */
function getValueAgainstFunction(numbers) {

	res 	= 	numbers[0] + 
				(numbers[1] * knownX) + 
				Math.pow(numbers[2] * knownX, 2) + 
				Math.pow(numbers[3] * knownX, 3) + 
				Math.pow(numbers[4] * knownX, 4) + 
				Math.pow(numbers[5] * knownX, 5); 

	setLowestAndHighestScores(res);

	return res;
}

/**
 * Checks a solution, and updates lowest and highest scores depending on
 * whether the solutions score is higher or lesser than current highest
 * and lowest scores
 */
function setLowestAndHighestScores(res) {

	if(lowestScore == 0) {

		lowestScore = res;
	
	} else if(res < lowestScore) {

		lowestScore = res;
	}

	if(res > highestScore) {

		highestScore = res;
	}
}

/**
 * Returns the fitness of a solution
 */
function getFitness(value) {

	// run against the polynomial function
	
	fitness = Math.sqrt(value) / Math.sqrt(knownY);

	return fitness;
}


$(function(){

	$('#start').click(function(event){

		numberOfRandomChromosomes 	= 	10;
		numberOfGenesPerChromosome	= 	6;
		numberOfBitsPerGene 		= 	16;
		totalBits 					= 	numberOfGenesPerChromosome * numberOfBitsPerGene;
		maxIterations 				= 	1000;
		solutionFound 				= 	false;
		debug						= 	false;
		lowestScore 				= 	0;
		highestScore 				= 	0;
		fittestChromosome 			= 	0;
		fittestSolution	 			=	{};
		numberOfBitsToCrossover		= 	50;
		fittestChromosomeFitness 	= 	0;
		tournamentSelectionAmount 	= 	10;
		numberOf1BitCrossovers 		= 	30; // note.. if set to 2.. this will generate
											// 4 new mutations, as each 1 bit crossover
											// generates 2 new mutations

		knownX 						= 	0.16837284482758727; 
											// this value is a mock 
											// value.. taken from some known data
		knownY 						= 	841.7108319251483; // this value is a mock value
		 									// that we know is returned when knownX is set
		 									// as above

		allUsedChromosomes 			= 	{};
		bestSolution 				= 	{}; 	// we will constantly update this as we
											// we move through each population.. if
											// we never find a winner.. then this will
											// be our best result 

		// start by getting some random chromosomes

		randomChromosomes = getRandomChromosomes(
			
			numberOfRandomChromosomes, 
			numberOfGenesPerChromosome,
			numberOfBitsPerGene
		);

		iteration 		= 0;

		bestSolution 	= startGeneticAlgorithm(randomChromosomes);

		if(debug) {

			console.log("Highest value: " + highestScore);
			console.log("Lowest value: " + lowestScore);

		}

	});

})